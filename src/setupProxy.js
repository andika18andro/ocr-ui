const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {

    app.use(
        '/v1',
        createProxyMiddleware({
            target: 'https://mage.uibot.com.cn',
            headers: {
                "Connection": "keep-alive"
            },
            changeOrigin: true,
            secure: false
        })
    );
};
