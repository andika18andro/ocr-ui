import React, { useState } from "react";
import { Box, Button, Grid, LinearProgress, Paper, TextField, Typography } from '@material-ui/core';
import ImageUpload from '../components/widgets/ImageUpload';
import { isEmpty } from "lodash";

export default function Home() {

    const [data, setData] = useState({ pictures: [], scanResult: [] });
    const [uiState, setUiState] = useState({ loading: false });

    const scan = () => {
        if (data.pictures.length > 0) {
            const files = data.pictures[0];
            var fr = new FileReader();
            fr.onload = function () {
                setUiState({ loading: true });

                var base64 = fr.result;

                const tmp = base64.split(',');
                if (tmp.length > 1) {
                    base64 = tmp[1]
                }

                const postData = {
                    "with_struct_info": true,
                    "with_raw_info": true,
                    "img_base64": base64
                }

                fetch(process.env.REACT_APP_MAGE_URL, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                        'Api-Auth-pubkey': '3oBznlcRLBKzEJkz8qSVraA1',
                        'Api-Auth-timestamp': '1612780838',
                        'Api-Auth-nonce': 'testMLPT',
                        'Api-Auth-sign': 'da3623a87b59e0ac02306ffe62347c03fe2d0d88',
                        'Api-Auth-access-token': '6YQrK9QlIxRxMcmrQrQZPbv5HdQrPDCs',
                    },
                    body: JSON.stringify(postData),
                }).then(res => res.json()).then(json => {
                    console.log('Success:', json);
                    if (!isEmpty(json.data.results)) {
                        setData({ ...data, scanResult: json.data.results })
                    }

                    setUiState({ loading: false });
                }).catch((err) => {
                    console.error('Error:', err);
                    setUiState({ loading: false });
                });
            }
            fr.readAsDataURL(files[0]);
        }
    }

    const renderResult = (result) => {

        const resultElement = [];

        result.forEach((item, idx) => {

            let itemResult = "";
            if (item.results.length > 0) {
                itemResult = item.results[0];
            }

            resultElement.push(
                <Grid item md={12} sm={12} style={{ margin: '10px' }}>
                    <TextField
                        id={`result-${idx}`}
                        label={item.field_name}
                        type="text"
                        value={itemResult}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        fullWidth
                        disabled={true}
                    />
                </Grid>
            );
        });

        return (
            <Grid container direction={"column"}>
                {resultElement}
            </Grid>
        );
    }

    if (uiState.loading) {
        return (
            <Grid container direction={"row"} spacing={0} justify={"center"}>
                <Grid item md={6}>
                    <Grid container direction={"column"} spacing={0} justify={"center"}>
                        <Grid item md={12} justify={"center"}>
                            <Grid container style={{ padding: 20 }}>
                                <ImageUpload data={data} setData={setData}></ImageUpload>
                            </Grid>
                        </Grid>
                        <Grid item md={12}>
                            <Grid container style={{ margin: 20 }} justify={"center"}>
                                <Button variant="contained" color="primary" onClick={scan} disabled={uiState.loading}> Upload </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={6} style={{ padding: 20 }}>
                    <Paper variant="outlined" elevation={3} style={{ padding: 30, minWidth: "100%" }}>
                        <Grid container direction={"column"} justify={"center"}>
                            <Grid item md={12}>
                                <LinearProgress style={{ height: "2px", marginTop: 20 }} />
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        )
    } else {
        if (!isEmpty(data.scanResult)) {
            return (
                <Grid container direction={"row"} spacing={0} justify={"center"}>
                    <Grid item md={6}>
                        <Grid container direction={"column"} spacing={0} justify={"center"}>
                            <Grid item md={12} justify={"center"}>
                                <Grid container style={{ padding: 20 }}>
                                    <ImageUpload data={data} setData={setData}></ImageUpload>
                                </Grid>
                            </Grid>
                            <Grid item md={12}>
                                <Grid container style={{ padding: 20 }} justify={"center"}>
                                    <Button variant="contained" color="primary" onClick={scan} disabled={uiState.loading}> Upload </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item md={6} style={{ padding: 30 }}>
                        <Paper variant="outlined" elevation={3} overflow={"visible"} style={{ padding: 30, minWidth: "100%" }}>
                            <Grid container direction={"column"} justify={"center"}>
                                <Grid item md={12} >
                                    <Grid container style={{ padding: 20 }} justify={"center"}>
                                        <Typography variant="h5" gutterBottom>
                                            Result
                                    </Typography>
                                    </Grid>
                                </Grid>
                                <Grid item md={12} >
                                    <Box component="div" my={2} overflow="auto" bgcolor="background.paper" style={{ maxHeight: 400 }}>
                                        {renderResult(data.scanResult)}
                                    </Box>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            )
        } else {
            return (
                <Grid container direction={"row"} justify={"center"}>
                    <Grid item md={6}>
                        <Grid container direction={"column"} justify={"center"}>
                            <Grid item md={12} justify={"center"}>
                                <Grid container style={{ padding: 20 }}>
                                    <ImageUpload data={data} setData={setData}></ImageUpload>
                                </Grid>
                            </Grid>
                            <Grid item md={12}>
                                <Grid container style={{ padding: 20 }} justify={"center"}>
                                    <Button variant="contained" color="primary" onClick={scan} disabled={uiState.loading}> Upload </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item md={6} style={{ padding: 30 }}>
                        <Paper style={{ padding: 30, minWidth: "100%" }}>
                            <Grid container direction={"column"} justify={"center"}>
                                <Grid item md={12}>
                                    <Grid container style={{ padding: 20 }} justify={"center"}>
                                        <Typography variant="h5" gutterBottom>
                                            Empty Result
                                    </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            )
        }


    }


}