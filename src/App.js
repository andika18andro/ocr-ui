import logo from './logo.svg';
import './App.css';
import Home from './pages/Home';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { createMuiTheme, LinearProgress, MuiThemeProvider } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import React, { Suspense } from 'react';

const theme = createMuiTheme({
  typography: {
    "fontFamily": `"Quicksand", sans-serif`,
  },
});

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <Router basename={process.env.PUBLIC_URL}>
        <Suspense fallback={<LinearProgress style={{ maxHeight: "2px" }} />}>
          <Switch>
            <Route key="home" path="/" exact render={() => <Home />} />
          </Switch>
        </Suspense>
      </Router>
    </MuiThemeProvider>
  );
}

export default App;
