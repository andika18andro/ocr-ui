import { cloneDeep } from "lodash";
import ImageUploader from 'react-images-upload';

export default function ImageUpload(props) {

    const onDrop = (image) => {
        const tmp = cloneDeep(props.data.pictures);
        props.setData({ pictures: [...tmp, image] });
    }

    return (
            <ImageUploader
                withPreview={true}
                withIcon={true}
                buttonText='Choose image'
                onChange={onDrop}
                imgExtension={['.jpg', '.jpeg', '.gif', '.png', '.gif']}
                maxFileSize={5242880}
                singleImage={true}
            />

    );
}